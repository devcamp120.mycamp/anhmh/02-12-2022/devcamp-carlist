const gJsonCars = `[{"make":"Ford","model":"Explorer","year":2017,"color":"white","vID":"AB-10111"},{"make":"Toyota","model":"Corolla","year":2018,"color":"silver","vID":"DN-23218"},{"make":"Mazda","model":"Mazda 6","year":2020,"color":"red","vID":"TZ-23212"},{"make":"Toyota","model":"Fortuna","year":2016,"color":"black","vID":"IN-91925"},{"make":"Mazda","model":"Mazda 3","year":2019,"color":"silver","vID":"MN-44593"}]`;
const gJsonCarsToObject = JSON.parse(gJsonCars);
console.log(gJsonCarsToObject);
import {gCarObject} from './info';

function App() {
  return (
    <div className="App">
      <ul>{gCarObject.car_0.map(function(pElement, pIndex){
            return<li>{pElement}</li>
          })}
          <p><li>{gCarObject.year_0 < 2018 ? "Ô tô cũ":"Ô tô mới"}</li></p>
      </ul>
      
      <ul>
      {gCarObject.car_1.map(function(pElement, pIndex){
            return<li>{pElement}</li>
          })}
          <p><li>{ gCarObject.year_1 < 2018 ?  "Ô tô cũ":"Ô tô mới"}</li></p>
      </ul>
      <ul>
      {gCarObject.car_2.map(function(pElement, pIndex){
            return<li>{pElement}</li>
          })}
          <p><li>{gCarObject.year_2 < 2018 ? "Ô tô cũ":"Ô tô mới"}</li></p>
      </ul>
      <ul>
      {gCarObject.car_3.map(function(pElement, pIndex){
            return<li>{pElement}</li>
          })}
           <p><li>{ gCarObject.year_3 < 2018 ? "Ô tô cũ":"Ô tô mới"}</li></p>
      </ul>
      <ul>
      {gCarObject.car_4.map(function(pElement, pIndex){
            return<li>{pElement}</li>
          })}
           <p><li>{gCarObject.year_4 < 2018 ? "Ô tô cũ":"Ô tô mới"}</li></p>
      </ul>
    </div>
  );
}

export default App;
/*
const gCarOneObject = {
  color: "white",
  make: "Ford",
  model: "Explorer",
  vID: "AB-10111",
  year: 2017
}
color: "silver"
make: "Toyota"
model: "Corolla"
vID: "DN-23218"
year: 2018
*/
