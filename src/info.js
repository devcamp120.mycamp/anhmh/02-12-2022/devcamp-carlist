export const gCarObject = {
    car_0: ['Ford, AB-10111'],
    year_0: 2017,
    car_1: ['Toyota, DN-23218'],
    year_1: 2018,
    car_2: ['Mazda, TZ-23212'],
    year_2: 2020,
    car_3: ['Toyota, IN-91925'],
    year_3: 2016,
    car_4: ['Mazda, MN-44593'],
    year_4: 2019
}